package com.vkram2711.snake

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.google.gson.Gson
import com.vkram2711.snake.callbacks.SnakeAndFoodCollisionCallback
import com.vkram2711.snake.gameObjects.*
import com.vkram2711.snake.models.SnakeBodyModel
import com.vkram2711.snake.models.SnakeModel
import java.util.*
import kotlin.collections.ArrayList
import kotlin.jvm.JvmOverloads

/* compiled from: GameFieldView.kt */

class GameFieldView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0): SurfaceView(context, attrs, defStyleAttr), SurfaceHolder.Callback, Runnable {


    private var bodyBitmap: Bitmap = Utils.getBitmap(context, R.drawable.body)
    private lateinit var canvas: Canvas
    private var food: Food? = null
    private lateinit var gameThread: Thread
    lateinit var onCollisionCallback: SnakeAndFoodCollisionCallback

    lateinit var snake: Snake
    private var paint: Paint
    private var playing: Boolean = false
    private var surfaceHolder: SurfaceHolder


    init {
        holder.addCallback(this)
        surfaceHolder = holder
        paint = Paint()
        getLastGame()
        resume()

        //Костыль не трогать
        pause()
        resume()
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {}
    override fun surfaceDestroyed(holder: SurfaceHolder?) {}

    override fun surfaceCreated (surfaceHolder: SurfaceHolder) {
         this.surfaceHolder = surfaceHolder
    }

    override fun run() {
        while (playing) {
            update()
            draw()
            control()
        }
    }

    fun pause() {
        playing = false
        gameThread.join()
        val arrayList: ArrayList<SnakeBodyModel> = ArrayList()

        for (i in 0 until snake.snake.size) arrayList.add(
            SnakeBodyModel(
                snake.snake[i].x,
                snake.snake[i].y,
                snake.snake[i].lastX,
                snake.snake[i].lastY
            )
        )


        val edit = context.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, 0).edit()

        var x = 0
        var y = 0
        if(food != null) {
            x = food!!.x
            y = food!!.y
        }
        edit.putString(Constants.SHARED_PREFERENCES_KEY, Gson().toJson(
            SnakeModel(
                x,
                y,
                snake.headX,
                snake.headY,
                MainActivity.score,
                arrayList,
                snake.direction
            )
        )).apply()
    }

    fun restart() {
        val gameOverCallback = snake.onGameOverCallback
        snake = Snake(context, 0, 0, Constants.DIRECTION_CODE_SOUTH, ArrayList(), width, height )
        snake.rotateHead()


        snake.onGameOverCallback = gameOverCallback
        MainActivity.score = 0
        createFood()
        resume()
    }

    fun resume() {
        playing = true
        gameThread = Thread(this)

        gameThread.start()
    }

    private fun update() {
        if (width > 0) {
            snake.width = width
            snake.height = height
            if(food == null) createFood()
        }

        snake.update()
        if (food != null) {

            if (Rect.intersects(food!!.detectCollision, snake.detectCollision)) {
                   MainActivity.score ++
                snake.addBody()
                createFood()
                onCollisionCallback.onCollision()
            }
        }
    }

    private fun createFood() {
        val x = Random().nextInt(width - 40)
        val y =  Random().nextInt(height - 40)
        val foodBitmap = Utils.getBitmap(context, R.drawable.food)
        food = Food(context, x, y, foodBitmap, Rect(x, y, x + foodBitmap.width,y + foodBitmap.height ))//null, 24, null);
    }

    private fun draw() {
        if(surfaceHolder.surface.isValid) {
            val can = surfaceHolder.lockCanvas()

            if(can != null) {
                canvas = can

                val surface = surfaceHolder.surface
                if (surface.isValid) {
                    canvas.drawColor(-16777216)
                    paint.color = -1


                    val headBitmap = snake.headBitmap

                    canvas.drawBitmap(headBitmap, snake.headX.toFloat(), snake.headY.toFloat(), this.paint)
                    if (this.food != null) {

                        canvas.drawBitmap(
                            food!!.foodBitmap,// snake.headX.toFloat(),
                            food!!.x.toFloat(),
                            food!!.y.toFloat(),
                            paint
                        )
                    }

                    for (i in 0 until snake.snake.size) {

                        canvas.drawBitmap(
                            snake.snake[i].body,
                            snake.snake[i].x.toFloat(),
                            snake.snake[i].y.toFloat(),
                            paint
                        )
                    }

                    surfaceHolder.unlockCanvasAndPost(canvas)
                }
            }
        }
    }

    private fun control() {
        Thread.sleep(50)
    }

    private fun getLastGame() {
        /*val sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)

        if (sharedPreferences.contains(Constants.SHARED_PREFERENCES_KEY)) {
            val snakeModel = Gson().fromJson(sharedPreferences.getString(Constants.SHARED_PREFERENCES_KEY, ""), SnakeModel::class.java)
            val snakeBodies: ArrayList<SnakeBody> = ArrayList()
            for(i in 0 until  snakeModel.snakeBodies.size){
                snakeBodies.add(SnakeBody(bodyBitmap, snakeModel.snakeBodies[i].x, snakeModel.snakeBodies[i].y, snakeModel.snakeBodies[i].lastX, snakeModel.snakeBodies[i].lastY, Rect()))
            }


            snake = Snake(context, snakeModel.headX, snakeModel.headY, snakeModel.snakeDirection, snakeBodies, width, height)
            val foodBitmap = Utils.getBitmap(context, R.drawable.food)
            food =  Food(context, snakeModel.foodX, snakeModel.foodY, foodBitmap, Rect(snakeModel.foodX, snakeModel.foodY, snakeModel.foodX + foodBitmap.width,snakeModel.foodY + foodBitmap.height ))
            MainActivity.score = snakeModel.score
            return
        }*/

        snake = Snake(context, 0,0, Constants.DIRECTION_CODE_SOUTH, ArrayList(), height, width)
        snake.rotateHead()
     }
}
