package com.vkram2711.snake.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vkram2711.snake.R
import com.vkram2711.snake.models.TopGameModel
import com.vkram2711.snake.models.TopGamesModel
import kotlinx.android.synthetic.main.top_games_item.view.*

class TopGamesAdapter(private val topGames: TopGamesModel): RecyclerView.Adapter<TopGamesAdapter.Holder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) = Holder(LayoutInflater.from(p0.context).inflate(R.layout.top_games_item, p0, false))

    override fun getItemCount() = topGames.topGames.size

    override fun onBindViewHolder(p0: Holder, p1: Int) = p0.bind(topGames.topGames[p1])

    inner class Holder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(topGame: TopGameModel){
            itemView.position.text = (adapterPosition + 1).toString()
            itemView.name.text = topGame.name
            itemView.score.text = topGame.score.toString()
        }
    }
}