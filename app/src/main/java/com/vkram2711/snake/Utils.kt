package com.vkram2711.snake


import android.content.Context
import android.graphics.Bitmap
import android.graphics.Bitmap.Config
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.VectorDrawable
import android.support.v4.content.ContextCompat
import com.vkram2711.snake.models.TopGameModel
import com.vkram2711.snake.models.TopGamesModel

class Utils {
    companion object {
        private fun getBitmap(vectorDrawable: VectorDrawable): Bitmap {
            val createBitmap =
                Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Config.ARGB_8888)
            val canvas = Canvas(createBitmap)
            vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
            vectorDrawable.draw(canvas)
            return createBitmap
        }

        fun getBitmap(context: Context, i: Int): Bitmap {
            val drawable = ContextCompat.getDrawable(context, i)
            return if (drawable is BitmapDrawable) {
                BitmapFactory.decodeResource(context.resources, i)
            } else {
                val vectorDrawable = drawable as VectorDrawable?
                getBitmap(vectorDrawable!!)
            }
        }
        
        fun addTopGame(currentGame: TopGameModel, topGamesModel: TopGamesModel): TopGamesModel {
            topGamesModel.topGames.add(currentGame)

            for (i in 0 until topGamesModel.topGames.size - 1) {
                for (j in 0 until topGamesModel.topGames.size - i - 1) {
                    if(topGamesModel.topGames[j].score < topGamesModel.topGames[j + 1].score){
                        val tmp = topGamesModel.topGames[j]
                        topGamesModel.topGames[j] = topGamesModel.topGames[j + 1]
                        topGamesModel.topGames[j + 1] = tmp
                    }
                }
            }

            if(topGamesModel.topGames.size > 10) topGamesModel.topGames.removeAt(topGamesModel.topGames.lastIndex)

            return topGamesModel
        }
    }
}

