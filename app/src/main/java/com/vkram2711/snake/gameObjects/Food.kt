package com.vkram2711.snake.gameObjects

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Rect

data class Food(val context: Context,  val x: Int, val y: Int,  val foodBitmap: Bitmap, val detectCollision: Rect)