package com.vkram2711.snake.gameObjects

import android.graphics.Bitmap
import android.graphics.Rect

data class SnakeBody(val body: Bitmap,
                     var x: Int,
                     var y: Int,
                     var lastX: Int,
                     var lastY: Int,
                     var collision: Rect?)