package com.vkram2711.snake.gameObjects

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Rect
import com.vkram2711.snake.R
import com.vkram2711.snake.Utils
import com.vkram2711.snake.callbacks.OnGameOverCallback
import android.graphics.Matrix
import android.util.Log
import com.vkram2711.snake.MainActivity
import android.opengl.ETC1.getWidth




class Snake {
    private val TAG = this::class.java.simpleName
    val bodyBitmap: Bitmap
    var changeDirection: Boolean = false

    var headDefaultBitmap: Bitmap
    var detectCollision: Rect
    var direction: Int
    var headBitmap: Bitmap
    var headX: Int
    var headY: Int
    var height: Int
    var lastHeadX: Int
    var lastHeadY: Int
    var width: Int

    val snake: ArrayList<SnakeBody>
    lateinit var onGameOverCallback: OnGameOverCallback

    constructor (context: Context, headX: Int, headY: Int, direction: Int, snake: ArrayList<SnakeBody>, width: Int, height: Int){
        this.snake = snake
        this.headX = headX
        this.headY = headY
        this.direction = direction
        this.width = width
        this.height = height
        headDefaultBitmap = Utils.getBitmap(context, R.drawable.head)
        bodyBitmap = Utils.getBitmap(context, R.drawable.body)
        headBitmap = headDefaultBitmap
        detectCollision = Rect(headX, headY, headX + headDefaultBitmap.width, headY + headDefaultBitmap.height)
        lastHeadX = headX
        lastHeadY = headY
    }

    fun update() {
        lastHeadX = headX
        lastHeadY = headY
        moveSnakeHead()
        if (headX < 0) {
            headX = width
        }
        if (headX > width && width > -1) {
            headX = 0
        }
        if (headY < 0) {
            headY = height
        }
        if (headY > height && height > -1) {
            headY = 0
        }

        detectCollision = Rect(headX, headY, headX + headBitmap.width, headY + headBitmap.height)
        for (i in 0 until snake.size) {
            snake[i].lastX = snake[i].x
            snake[i].lastY = snake[i].y

            if (changeDirection) moveSnakeBody(i)

            if (i > 0) {
                snake[i].x = snake[i - 1].lastX
                snake[i].y = snake[i - 1].lastY
            } else {
                snake[i].x = lastHeadX
                snake[i].y = lastHeadY
            }
            if (snake[i].x < -30) {
                snake[i].x = width
            }
            if (snake[i].x > width) {
                snake[i].x = 0
            }
            if (snake[i].y < -30) {
                snake[i].y = height
            }
            if (snake[i].y > height) {
                snake[i].y = 0
            }


            snake[i].collision = Rect(
                    snake[i].x,
                    snake[i].y,
                    snake[i].x + snake[i].body.width,
                    snake[i].y + snake[i].body.height
                )


            Log.d(TAG, "head ${detectCollision.top}, ${detectCollision.left}, ${detectCollision.right}, ${detectCollision.bottom}")
            Log.d(TAG, "snake ${snake[i].collision!!.top}, ${snake[i].collision!!.left}, ${snake[i].collision!!.right}, ${snake[i].collision!!.bottom}")

            if (Rect.intersects(detectCollision, snake[i].collision) && "head ${detectCollision.top}, ${detectCollision.left}, ${detectCollision.right}, ${detectCollision.bottom}" != "head 0, 0, 49, 60" && "snake ${snake[i].collision!!.top}, ${snake[i].collision!!.left}, ${snake[i].collision!!.right}, ${snake[i].collision!!.bottom}" != "snake 0, 0, 52, 49") {
                if(::onGameOverCallback.isInitialized) {
                    if(i > 2 && width > 0) onGameOverCallback.onGameOver()
                }
            }
        }

        if(snake.isEmpty()){
            for (i in 0 until 5) {
                addBody()
            }
        }
    }

    fun addBody() {
        if(snake.isNotEmpty())
        snake.add(
            SnakeBody(
                bodyBitmap,

                snake.last().x - headBitmap.width,
                snake.last().y - headBitmap.height,
                snake.last().x - headBitmap.width,
                snake.last().y - headBitmap.height,
            null)
        )
        else snake.add(
            SnakeBody(
                    bodyBitmap,
                    -50 - headBitmap.width,
                    -50,
                   -50,
                    -50,
            null
                )
            )
    }

    fun rotateHead(){
        when (this.direction) {
            0 -> headBitmap = rotate(headDefaultBitmap, -90.0f)
            1 -> headBitmap = rotate(headDefaultBitmap, 0.0f)
            2 -> headBitmap = rotate(headDefaultBitmap, 90.0f)
            3 -> headBitmap = rotate(headDefaultBitmap, 180.0f)
        }
    }

    fun rotate( bitmap: Bitmap, f: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(f)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    private fun moveSnakeHead() {
        when (direction) {
            0 -> {
                headY -= headBitmap.height / 2
            }
            1 -> {
                headX += headBitmap.height / 2
            }
            2 -> {
                headY += headBitmap.height / 2
            }
            3 -> {
                headX -= headBitmap.height / 2
            }
        }
    }

    private fun moveSnakeBody(i: Int) {
        val snakeBody = snake[i]
        when (direction) {
            0 -> if (i != 0) {
                snakeBody.x = snake[i - 1].lastX
            } else {
                snakeBody.x = headX
            }
            1 -> if (i != 0) {
                snakeBody.y = snake[i - 1].lastY
            } else {
                snakeBody.y = headY
            }
            2 -> if (i != 0) {
                snakeBody.x = snake[i - 1].lastX
            } else{
                snakeBody.x = headX
            }
            3 -> if (i != 0) {
                snakeBody.y = snake[i - 1].lastY

            } else {
                snakeBody.y = headY
            }

        }
        changeDirection = false
    }
}