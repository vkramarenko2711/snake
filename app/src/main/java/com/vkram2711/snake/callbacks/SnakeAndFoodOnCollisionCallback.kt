package com.vkram2711.snake.callbacks

interface SnakeAndFoodCollisionCallback {
    fun onCollision()
}
