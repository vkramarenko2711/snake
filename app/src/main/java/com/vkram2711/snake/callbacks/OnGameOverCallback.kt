package com.vkram2711.snake.callbacks

interface OnGameOverCallback {
    fun onGameOver()
}

