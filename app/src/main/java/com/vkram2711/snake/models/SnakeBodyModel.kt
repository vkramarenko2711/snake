package com.vkram2711.snake.models

data class SnakeBodyModel(var x: Int, var y: Int, var lastX: Int, var lastY: Int)