package com.vkram2711.snake.models

data class TopGameModel(val name: String, val score: Int)