package com.vkram2711.snake.models

data class SnakeModel(var foodX: Int,
                      var foodY: Int,
                      var headX: Int,
                      var headY: Int,
                      var score: Int,
                      val snakeBodies: ArrayList<SnakeBodyModel>,
                      var snakeDirection: Int)