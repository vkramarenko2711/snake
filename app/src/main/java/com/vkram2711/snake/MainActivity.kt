package com.vkram2711.snake

import android.content.Context
import android.content.pm.ActivityInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.vkram2711.snake.adapters.TopGamesAdapter
import com.vkram2711.snake.callbacks.OnGameOverCallback
import com.vkram2711.snake.callbacks.SnakeAndFoodCollisionCallback
import com.vkram2711.snake.models.TopGameModel
import com.vkram2711.snake.models.TopGamesModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnGameOverCallback, SnakeAndFoodCollisionCallback {

    companion object {
        var score = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        userInterfaceInit()
    }

    override fun onGameOver() {
        runOnUiThread {
            game_over_layout.visibility = View.VISIBLE
            game_field.visibility = View.INVISIBLE
            game_field.pause()

            val sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)

            val currentGame = TopGameModel("Player", MainActivity.score)
            val topGames =  Utils.addTopGame(currentGame, if(sharedPref.contains("top games")) Gson().fromJson(sharedPref.getString("top games", ""), TopGamesModel::class.java) else TopGamesModel(ArrayList()))

            top_games_list.adapter = TopGamesAdapter(topGames)
            top_games_list.layoutManager = LinearLayoutManager(this)

            sharedPref.edit().putString("top games", Gson().toJson(topGames)).apply()
        }
    }


    override fun onCollision() {
        runOnUiThread {score.text = MainActivity.score.toString()}
    }

    override fun onKeyDown(i: Int, keyEvent: KeyEvent): Boolean {
        when (i) {
            24 -> {
                if (game_field.snake.direction == Constants.DIRECTION_CODE_NORTH) {
                    game_field.snake.direction =  Constants.DIRECTION_CODE_WEST
                } else {
                    game_field.snake.direction = game_field.snake.direction - 1
                }
                game_field.snake.changeDirection = true
                game_field.snake.rotateHead()
                return true
            }
            25 -> {
                if (game_field.snake.direction == Constants.DIRECTION_CODE_WEST ) {
                    game_field.snake.direction = Constants.DIRECTION_CODE_NORTH
                } else {
                    game_field.snake.direction = game_field.snake.direction + 1
                }
                game_field.snake.changeDirection = true
                game_field.snake.rotateHead()
                return true
            }
            else -> return super.onKeyDown(i, keyEvent)
        }
    }

    private fun userInterfaceInit(){
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        game_field.onCollisionCallback = this
        game_field.snake.onGameOverCallback = this

        pause.setOnClickListener {
            game_field.pause()
            pause_text.visibility = View.VISIBLE
            resume.visibility = View.VISIBLE
            game_field.visibility = View.INVISIBLE
        }

        resume.setOnClickListener {
            game_field.resume()
            pause_text.visibility = View.INVISIBLE
            resume.visibility = View.INVISIBLE
            game_field.visibility = View.VISIBLE
        }

        restart.setOnClickListener {
            game_field.restart()
            game_field.visibility = View.VISIBLE
            game_over_layout.visibility = View.INVISIBLE
            score.text = MainActivity.score.toString()
        }
    }

    override fun onResume() {
        super.onResume()
        game_field.resume()
    }

    override fun onPause() {
        super.onPause()
        game_field.pause()
    }
}
