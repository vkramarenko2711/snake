package com.vkram2711.snake

class Constants {
    companion object {
         const val DEFAULT_SNAKE_SIZE = 5
         const val DIRECTION_CODE_EAST = 1
         const val DIRECTION_CODE_NORTH = 0
         const val DIRECTION_CODE_SOUTH = 2
         const val DIRECTION_CODE_WEST = 3
         const val FIELD_BACKGROUND_COLOR = -16777216
         const val SHARED_PREFERENCES_KEY = "snakeKey"
         const val SHARED_PREFERENCES_NAME = "snake"
    }
}